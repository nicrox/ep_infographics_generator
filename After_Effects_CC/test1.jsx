﻿{
  app.beginUndoGroup("Demo Script");

  // Creating project
  var currentProject  = (app.project) ? app.project : app.newProject();

  // Creating comp
  var compSettings    = cs = [1280, 720, 1, 10, 24];
  var defaultCompName = "Demo"
  var currentComp     = (currentProject.activeItem) ? currentProject.activeItem : currentProject.items.addComp(defaultCompName, cs[0], cs[1], cs[2], cs[3], cs[4]);
  currentComp.openInViewer();

  // Creating background layer
  var backgroundLayer = currentComp.layers.addSolid([93, 5, 2], "Background", cs[0], cs[1], cs[2]);


  

  // Adding text layer
  var textLayer                   = currentComp.layers.addText("HOLA MUNDO!");
  var textProperty               = textLayer.property("Source Text");
  var textPropertyValue       = textProperty.value;
  
  // Changing source text settings
textPropertyValue.resetCharStyle();
textPropertyValue.fontSize      = 100;
textPropertyValue.fillColor     = [0, 0, 0];
textPropertyValue.justification = ParagraphJustification.CENTER_JUSTIFY;
textProperty.setValue(textPropertyValue);


// Adjusting text layer anchor point
var textLayerHeight             = textLayer.sourceRectAtTime(0, false);
textLayer.property("Anchor Point").setValue([0, textLayerHeight.height / 2 * -1])


  app.endUndoGroup();

}