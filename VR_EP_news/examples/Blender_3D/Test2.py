import bpy
import os

ROOTPATH='D:/Oproyects/PROYECTOS/New_VR_bitbucket/xmlData/'

#Definimos variables
coords=[]
faces=[]

x=-8
y=0
z=0

def MakeNew(Plocation,Pimg):
	# Creamos primitiva 
	coords=[[0,0,0],[1,0,0],[1,0.5,0],[0,0.5,0]]
	faces=[[0,1,2,3]]

	me = bpy.data.meshes.new("cuadrado")
	me.from_pydata(coords, [], faces)
	me.update()

	# Creamos objeto nuevo
	ob = bpy.data.objects.new('cuadrado', me)
	ob.data = me  # link the mesh data to the object

	# Asignamos material
	material = bpy.data.materials.new("white_material")
	print(">>Create white Material")

	material_texture = material.texture_slots.add()
	image_path = os.path.expanduser(ROOTPATH + Pimg)
	image = bpy.data.images.load(image_path)
	texture = bpy.data.textures.new("new_texture", type = 'IMAGE')
	texture.image = image
	# link texture and material
	material_texture.texture = texture
	material_texture.texture_coords = 'ORCO'
	material_texture.mapping = 'FLAT'
		
	ob.active_material = material

	# Cramos escena
	scene = bpy.context.scene   # get the current scene
	scene.objects.link(ob) # link the object into the scene

	ob.location = Plocation #  scene.cursor_location

#creamos copias recurentes
counter=0

while counter <=9:
	MakeNew([x,y,z],'img_pais'+str(counter)+'.jpg')
	x +=1.5
	counter +=1
