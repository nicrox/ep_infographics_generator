import bpy
import os

ROOTPATH='D:/Oproyects/GIT_proyects/New_VR/xmlData/'

# Creamos primitiva 
coords=[]
faces=[]
coords=[[0,0,0],[1,0,0],[1,1,0],[0,1,0]]
faces=[[0,1,2,3]]

me = bpy.data.meshes.new("cuadrado")
me.from_pydata(coords,[],faces)
me.update()

# Creamos objeto nuevo
ob = bpy.data.objects.new('cuadrado', me)
ob.data = me   # link the mesh data to the object

# Asignamos material
if bpy.data.materials.get("white_material") is not None:
	material = bpy.data.materials.new("white_material")
	print(">>Create white Material")
else:
	material = bpy.data.materials["white_material"] 
	print(">>white Material")	
	
material_texture = material.texture_slots.add()
image_path = os.path.expanduser(ROOTPATH + 'img_br10.jpg')
image = bpy.data.images.load(image_path)
texture = bpy.data.textures.new("new_texture", type = 'IMAGE')
texture.image = image
# link texture and material
material_texture.texture = texture
material_texture.texture_coords = 'ORCO'
material_texture.mapping = 'FLAT'
	

ob.active_material = material

# Cramos escena
scene = bpy.context.scene   # get the current scene
scene.objects.link(ob) # link the object into the scene

ob.location = [-0.5,-0.5,0] #  scene.cursor_location    