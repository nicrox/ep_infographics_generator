var express = require('express');

var port = 3000;
var server = express(); // better instead

  server.use('/xmlData', express.static(__dirname + '/xmlData'));
  server.use('/assets', express.static(__dirname + '/assets'));
  server.use(express.static(__dirname + '/public'));


server.listen(port);
console.log('Webserver active %s...',port);