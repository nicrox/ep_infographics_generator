/**
 * Created by operador on 17/07/2017.
 */
var https = require('https');
var xml2js = require('xml2js');
var parser = new xml2js.Parser();
var concat = require('concat-stream');
var fs = require('fs');
var Stream = require('stream').Transform;

var countImg=0;

/*salvamos las imagenes*/
var readFile = function (nameUrl, nameFile) {

    https.request(nameUrl, function (response) {
        var data = new Stream();

        response.on('data', function (chunk) {
            data.push(chunk);
            //console.log(chunk);
        });

        response.on('end', function () {
            fs.writeFileSync(nameFile, data.read());
            
			//countImg++;
			
        });
    }).end();
	

};


/*Invocamos la URl*/
var DataService = function(url,folder,file,imgData){
	

		parser.on('error', function(err) { console.log('Parser error', err); });

		https.get(url, function(resp) {

			resp.on('error', function(err) {
				console.log('Error while reading', err);
			});

			resp.pipe(concat(function(buffer) {
				var str = buffer.toString();
				parser.parseString(str, function (err, result) {
					//console.log('Finished parsing:', err, result);

					var jsonResult = JSON.stringify(result);

					//console.log(jsonResult);

					//fs.writeFile('xmlData/copia.json', jsonResult);  // Salvamos el xml en un json

					/*Creamos un nuevo json*/

					var state = {};
					var lib = JSON.parse(jsonResult);
			

					//console.log(lib.rss.channel[0].item[0]);
					for (i in lib.rss.channel[0].item) {

						/*
						console.log(lib.rss.channel[0].item[i].title);
						console.log(lib.rss.channel[0].item[i].description);
						console.log(lib.rss.channel[0].item[i].enclosure[0].$.url);
						console.log('---------------------------------------------');
						*/

						
						state[i] = {
							title: lib.rss.channel[0].item[i].title,
							description: lib.rss.channel[0].item[i].description,
							img: imgData +i+'.jpg'
						};

						/*Leemos y creamos las imagenes*/
						readFile(lib.rss.channel[0].item[i].enclosure[0].$.url,folder + imgData + i + '.jpg');
					}
					

					//console.log(state);
					fs.writeFile(folder + file, JSON.stringify(state));  // Salvamos el xml en un json


				});
			}));

		});
}


DataService('https://ep00.epimg.net/rss/elpais/portada.xml','xmlData/','portada_pais.json','img_pais');
DataService('https://ep00.epimg.net/rss/elpais/portada_america.xml','xmlData/','portada_america.json','img_ame');
DataService('https://ep00.epimg.net/rss/brasil/portada.xml','xmlData/','portada_brasil.json','img_br');

/* Otros */
DataService('https://ep00.epimg.net/rss/cat/portada.xml','xmlData/','portada_cat.json','img_cat');
//DataService('https://ep00.epimg.net/rss/ccaa/madrid.xml','xmlData/','portada_madrid.json','img_madrid');
//DataService('https://ep00.epimg.net/rss/ccaa/paisvasco.xml','xmlData/','portada_paisvasco.json','img_pv');




