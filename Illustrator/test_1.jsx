﻿// Exports current document to dest as a JPEG file with specified options,
// dest contains the full path including the file name
/*Hay que utilzar rutas absolutas para poder utilizar el script*/

app.activeDocument.filePath='C:/GITLAB/ep_infographics_generator/Illustrator/image';

//Alternativa D:/\Oproyects/\PROYECTOS/\ep_infographics_generator/\Illustrator/\image


function exportFileToJPEG(dest) {
  if (app.documents.length > 0) {
    var exportOptions = new ExportOptionsJPEG();
    exportOptions.antiAliasing = false;
    exportOptions.qualitySetting = 70;

    var type = ExportType.JPEG;
    var fileSpec = new File(dest);

    app.activeDocument.exportFile(fileSpec, type, exportOptions);
  }
}

exportFileToJPEG(app.activeDocument.filePath);
